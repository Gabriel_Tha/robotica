#include "naobehavior.h"
#include "../rvdraw/rvdraw.h"

extern int agentBodyType;


SkillType NaoBehavior::selectSkill() {
    return kickingAndStand();
}

SkillType NaoBehavior::kickingAndStand() {
    //return goToTarget(this->defaultPosition());
    const double MAX_DIST = 10000.0;
    // Find closest player to ball
    int playerClosestToBall = -1;
    double closestDistanceToBall = MAX_DIST;
    for(int i = WO_TEAMMATE1; i < WO_TEAMMATE1+NUM_AGENTS; ++i) {
        VecPosition temp;
        int playerNum = i - WO_TEAMMATE1 + 1;
        if (worldModel->getUNum() == playerNum) {
            // This is us
            temp = worldModel->getMyPosition();
        } else {
            WorldObject* teammate = worldModel->getWorldObject( i );
            // if (worldModel->getFallenTeammate(playerNum))
            //     cout << worldModel->getUNum() << ": Player " << playerNum << " is fallen" << endl;
            if (teammate->validPosition && worldModel->getFallenTeammate(playerNum) != true) {
                temp = teammate->pos;
            } else {
                continue;
            }
        }
        temp.setZ(0);
        double distanceToBall = temp.getDistanceTo(ball);

        if (distanceToBall < closestDistanceToBall) {
            if (worldModel->getUNum() == 1){
		VecPosition target;
		if ( (ball.getY() > -1.5 && ball.getY() < 1.5) && (ball.getX() < -HALF_FIELD_X + 2)){
            	   return kickBall(KICK_FORWARD, VecPosition(HALF_FIELD_X+1, 0, 0));
                }else if((ball.getX() < -HALF_FIELD_X+4.5) && (ball.getY() > -3.5 && ball.getY() < 3.5)) {
		    double tX = (-HALF_FIELD_X+ball.getX())/2.0;
		    double tY = ball.getY()/2.0;
		    target = VecPosition(tX, tY, 0);
		}else{
            	    target = this->defaultPosition();
		}
		return goToTarget(target);
            } else {
                playerClosestToBall = playerNum;
                closestDistanceToBall = distanceToBall;
            }
        }
    }

    if (playerClosestToBall == worldModel->getUNum()) {
        // Have closest player kick the ball toward the center
        VecPosition mypos = worldModel->getMyPosition();
        double close_dist = 0xff;
        for(int i = WO_OPPONENT1; i < WO_OPPONENT1+NUM_AGENTS; ++i) {
            WorldObject* opponent = worldModel->getWorldObject( i );
            int opponentNum = i - WO_OPPONENT1 + 1;
            double tmp;
            if (opponent->validPosition && worldModel->getFallenTeammate(opponentNum) != true) {
                tmp = mypos.getDistanceTo(opponent->pos);
            } else {
                tmp = 0xff;
            }
            if (close_dist > tmp && tmp!=0xff ){
                close_dist = tmp;
            }
        }
        if (close_dist < 1){
	     //std::cout << "usando kick_ik" << std::endl;
            return kickBall(KICK_IK, VecPosition(HALF_FIELD_X+1, 0, 0));
            //return kickBall(KICK_DRIBBLE, VecPosition(HALF_FIELD_X+1, 0, 0));
        } else if (close_dist < 1.55) {
	     //std::cout << "usando kick_forward" << std::endl;
            return kickBall(KICK_FORWARD, VecPosition(HALF_FIELD_X+1, 0, 0));
            //return kickBall(KICK_DRIBBLE, VecPosition(HALF_FIELD_X+1, 0, 0));
        } else {
	     //std::cout << "usando kick_new" << std::endl;
            return kickBall(KICK_NEW, VecPosition(HALF_FIELD_X+1, 0, 0));
            //return kickBall(KICK_DRIBBLE, VecPosition(HALF_FIELD_X+1, 0, 0));
	}
    } else {
        VecPosition mypos = worldModel->getMyPosition();
        double distanceToBall = mypos.getDistanceTo(ball);
        VecPosition target;
        /* se o cara ta perto da bola continua indo,
            nao volta pra posicao default */
        if (distanceToBall < 2){
                target = ball;
        }
        else{  
                int gameSituation = this->getGameSituation();
                int myNum = worldModel->getUNum();
                double yPos = mypos.getY();
                double yBall = ball.getY();
                bool bolaAtras =(yBall < yPos) ? true : false; 
                if(bolaAtras){
                    if(gameSituation == 4 || gameSituation == 6){
                        switch(myNum){
                            case 5:
                            case 6:
                            case 7:
                                target = ball;
                                break;
                            default:
                                target = this->defaultPosition();
                        }
                    }
                    else if(gameSituation == 5){
                        switch(myNum){
                            case 5:
                            case 6:
                                target = ball;
                                break;
                            default:
                                target = this->defaultPosition();
                        }
                    }
                    else {
                        switch(myNum){
                            case 4:
                            case 5:
                                target = ball;
                                break;
                            default:
                                target = this->defaultPosition();
                        }
                    }
                }else{
                    target = this->defaultPosition();
                }
        }
        //VecPosition target = this->defaultPosition();
        // Adjust target to not be too close to teammates or the ball
        target = collisionAvoidance(true /*teammate*/, false/*opponent*/, true/*ball*/, 1/*proximity thresh*/, .5/*collision thresh*/, target, true/*keepDistance*/);
        if (me.getDistanceTo(target) < .25) {
            // Close enough to desired position and orientation so just stand
            return SKILL_STAND;
        } else if (me.getDistanceTo(target) < .5) {
            // Close to desired position so start turning to face center
            return goToTargetRelative(worldModel->g2l(target), 0);
        } else {
            // Move toward target location
            return goToTarget(target);
        }
    }
}
